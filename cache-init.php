<?php

print '[' . date('d/M/Y:H:i:s O') . '] Prepare cache...' . PHP_EOL;

passthru('envsubst \'${ALFA_CONF_S3_ACCESS_KEY} ${ALFA_CONF_S3_HOST} ${ALFA_CONF_S3_SECRET_KEY}\' < /parser/s3cfg.template > ~/.s3cfg');

$output = '';
$result_code = NULL;
$cache_dir = $_ENV['ALFA_PARSER_CACHE_DIR'] . '/' . $_ENV['ALFA_ENV'];
if (!is_dir($cache_dir)) {
  mkdir($cache_dir, 0777, TRUE);
}
//$cmd = 's3cmd sync s3://' . $_ENV['ALFA_PARSER_CACHE_BUCKET'] . '/ ' . $cache_dir . '/';
//exec($cmd, $output, $result_code);

/*if ($result_code > 0) {
  print "Error on: " . $cmd;
  print PHP_EOL;
  print implode(PHP_EOL, $output);
  print PHP_EOL;
}*/

print '[' . date('d/M/Y:H:i:s O') . '] cache prepared.';
print PHP_EOL;
exit(0);