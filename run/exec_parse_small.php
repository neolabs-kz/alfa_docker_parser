<?php

shell_exec('php /parser/cache-init.php');

$_SERVER['HTTP_HOST'] = $_ENV['ALFA_DOMAIN_NAME'];
$_SERVER['DOCUMENT_ROOT'] = '/alfa';
chdir($_SERVER['DOCUMENT_ROOT']);
define('ALFA_ENV', isset($_ENV['ALFA_ENV'])? $_ENV['ALFA_ENV']: 'development');

include_once './includes/bootstrap.inc';
@ drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

//$prid = intval($argv[1]);

module_load_include('inc', 'aad_prices', 'aad_prices.autoparser');

ini_set('memory_limit', '4096M');
set_time_limit(0);

$queue = shell_exec('php /parser/run/get_parser_queue.php');
$d = new DOMDocument('1.0');
$d->loadXML($queue);

$result = 0;

$cache_dir = $_ENV['ALFA_PARSER_CACHE_DIR'] . '/' . ALFA_ENV;
$logs_dir = $_ENV['ALFA_PARSER_LOG_DIR'] . '/' . ALFA_ENV;
$log_name = date('Y-m-d') . '.log';

if (!is_dir($cache_dir)) {
  print 'Creating dir ' . $cache_dir . PHP_EOL;
  mkdir($cache_dir, 0777, TRUE);
}

foreach($d->getElementsByTagName('item') as $item) {
  if (!$item->getAttribute('is_big')) {
    $prid = $item->getAttribute('prid');

    $log_local_path = $logs_dir . '/' . $prid . '/' . $log_name;
    $log_remote_path = 's3://' . $_ENV['ALFA_PARSER_CACHE_BUCKET'] . '/logs/' . $prid . '/' . $log_name;
    if (!is_dir($log_dir = dirname($log_local_path))) {
      print 'Creating dir ' . $log_dir . PHP_EOL;
      mkdir($log_dir, 0777, TRUE);
    }


    print 'Parsing ' . $prid . PHP_EOL;

    if (!is_dir($cache_dir . '/' . $prid)) {
      mkdir($cache_dir . '/' . $prid);
    }
    $output = '';
    $result_code = NULL;
    // Синхронизируем объектное хранилище с локальной директорией, "скачиваем"
    $cmd = 's3cmd sync --preserve s3://' . $_ENV['ALFA_PARSER_CACHE_BUCKET'] . '/' . $prid . '/ ' . $cache_dir . '/' . $prid . '/';
    exec($cmd, $output, $result_code);

    if ($result_code > 0) {
      print "Error on: " . $cmd;
      print PHP_EOL;
      print implode(PHP_EOL, $output);
      print PHP_EOL;
      $result = $result_code;
    }

    // Скачиваем журнал парсера
    $cmd = 's3cmd get ' . $log_remote_path . ' ' . $log_local_path . ' --force';
    exec($cmd, $output, $result_code);

    if (!file_exists($cache_dir . '/' . $prid . '/shops.dtd')) {
      // DTD-файл необходим для валидации YML-документов.
      print 'copying ' . dirname(__FILE__) . '/shops.dtd to ' . $cache_dir . '/' . $prid . '/shops.dtd' . PHP_EOL;
      copy(dirname(__FILE__) . '/../shops.dtd', $cache_dir . '/' . $prid . '/shops.dtd');
    }

    $result_code = 0;
    passthru('php /parser/run/exec_parse.php ' . $prid, $result_code);
    if ($result_code != 0) {
      $result = $result_code;
    }

    // Синхронизируем локальную директорию с хранилищем, "закачиваем"
    $cmd = 's3cmd sync --preserve --exclude "*" --include "*.xml" ' . $cache_dir . '/' . $prid . '/ s3://' . $_ENV['ALFA_PARSER_CACHE_BUCKET'] . '/' . $prid . '/';
    exec($cmd, $output, $result_code);

    if ($result_code > 0) {
      print "Error on: " . $cmd;
      print PHP_EOL;
      print implode(PHP_EOL, $output);
      print PHP_EOL;
    }

    if ($result_code != 0) {
      $result = $result_code;
    }

    // Загружаем обратно журнал парсера
    $cmd = 's3cmd put ' . $log_local_path . ' ' . $log_remote_path . ' --force --acl-public';
    exec($cmd, $output, $result_code);
    $cmd = 's3cmd modify --add-header=\'Content-Type: text/plain; charset=utf-8\' ' . $log_remote_path;
    exec($cmd, $output, $result_code);

    // Очищаем локальную директорию, экономим место на диске, очень важная вещь
    $cmd = 'rm -rf ' . $cache_dir . '/' . $prid . '/';
    exec($cmd, $output, $result_code);
    if ($result_code > 0) {
      print "Error on: " . $cmd;
      print PHP_EOL;
      print implode(PHP_EOL, $output);
      print PHP_EOL;
    }

    if ($result_code != 0) {
      $result = $result_code;
    }
  }
}

exit($result);