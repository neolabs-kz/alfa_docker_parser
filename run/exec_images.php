<?php

$_SERVER['HTTP_HOST'] = $_ENV['ALFA_DOMAIN_NAME'];
$_SERVER['DOCUMENT_ROOT'] = '/alfa';
chdir($_SERVER['DOCUMENT_ROOT']);
define('ALFA_ENV', isset($_ENV['ALFA_ENV'])? $_ENV['ALFA_ENV']: 'development');

include_once './includes/bootstrap.inc';
@ drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$proc_id = intval($argv[1]);

module_load_include('inc', 'aad_prices', 'aad_prices.autoparser');

ini_set('memory_limit', '4096M');
set_time_limit(1800);

print autoparser_parse_images();