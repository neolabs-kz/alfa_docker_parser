<?php

$_SERVER['HTTP_HOST'] = $_ENV['ALFA_DOMAIN_NAME'];
$_SERVER['DOCUMENT_ROOT'] = '/alfa';
chdir($_SERVER['DOCUMENT_ROOT']);
define('ALFA_ENV', isset($_ENV['ALFA_ENV'])? $_ENV['ALFA_ENV']: 'development');

include_once './includes/bootstrap.inc';
@ drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

module_load_include('inc', 'aad_prices', 'aad_prices.autoparser');

print '<?xml version="1.0" encoding="utf-8"?>'; print PHP_EOL;

// Если осталось менее PRICES_AUTO_TIME_TRESHOLD секунд на выполнение, то прерываем процесс, он будет запущен следующим запуском крона
/*if (PRICES_AUTO_TIME_LIMIT - (time() - $parser_start_time) < PRICES_AUTO_TIME_TRESHOLD) {
  $time_left = PRICES_AUTO_TIME_LIMIT - (time() - $parser_start_time);
  print '<error>Out of time (only $time_left left)</error>';
  print PHP_EOL;
  autoparser_log("Out of time (only $time_left left) Mem usage: " . floor(memory_get_usage() / (1024*1024)) . 'M. Prices parsed: ' . $prices_parsed . ', list: ' . implode(', ', $prices_parsed_list), 0);
  exit;
}*/
// Если осталось менее PRICES_AUTO_MEMORY_TRESHOLD памяти свободно
/*if ( PRICES_AUTO_MEMORY_LIMIT - floor(memory_get_usage() / (1024*1024)) < PRICES_AUTO_MEMORY_TRESHOLD) {
  $memory_left = PRICES_AUTO_MEMORY_LIMIT - floor(memory_get_usage() / 1024*1024);
  print '<error>Out of memory (only $memory_left left)</error>';
  autoparser_log("Out of memory (only $memory_left left). Elapsed time: " . (time() - $parser_start_time) . '. Prices parsed: ' . $prices_parsed . ', list: ' . implode(', ', $prices_parsed_list), 0);
  exit;
}*/

/*if(isset($_GET['force_price'])) {
  $r = db_query('SELECT * FROM {aad_prices_auto} WHERE enabled = 1 AND prid = %d', $_GET['force_price']);
}
else if(isset($_GET['parse_big'])) {
  // Парсинг больших прайсов

  if (db_result(db_query("SELECT COUNT(*) FROM {aad_prices_process_queue}")) > 10000) {
    // Даём парсеру передышку
    autoparser_log("Парсеру больших прайсов нужно время на обработку, скопилась большая очередь.", 0);
    print "Парсеру больших прайсов нужно время на обработку, скопилась большая очередь.";
    exit;
  }

  // Даём другим обработкам время
  sleep($_GET['proc_id']);*/

$queue = array();

$time = time();
// Формируем очередь
$r = db_query('SELECT * FROM {aad_prices_auto} WHERE 
    enabled = 1 AND halted = 0 AND 
    (SELECT prid FROM {aad_prices} WHERE prid = {aad_prices_auto}.prid AND disabled = 0) IS NOT NULL AND 
    (working = 0 OR working < %d) AND 

    (
      bid = 0 OR 
      (
        bid > 0 AND
        working < %d AND
        working > %d AND
        (SELECT bid FROM {batch} WHERE bid = {aad_prices_auto}.bid) IS NOT NULL AND
        (SELECT COUNT(*) FROM {aad_prices_batch_result} WHERE bid = {aad_prices_auto}.bid) > 0 AND 
        (SELECT COUNT(*) FROM {aad_prices_process_queue} WHERE prid = {aad_prices_auto}.prid) = 0
      )
    ) AND 
    last_update < %d AND 
    (next_try < now() OR next_try IS NULL)
    ORDER BY bid > 0 DESC, last_update ASC',

    $time - PRICES_AUTO_WORKING_TIMEOUT,
    $time - PRICES_AUTO_BATCH_IDLE_TIMEOUT,
    $time - PRICES_AUTO_BATCH_IDLE_TIMEOUT_UPPER,
    $time - PRICES_AUTO_DELAY_BETWEEN_PARSE);

while($ar = db_fetch_array($r)) {
  $queue[] = array('prid' => $ar['prid'], 'is_big' => $ar['is_big'], 'bid' => $ar['bid']);
}

print '<queue>'; print PHP_EOL;

foreach($queue as $item) {
  print '<item prid="' . $item['prid'] . '" is_big="' . $item['is_big'] . '"' . (empty($item['bid'])? '': ' bid="' . $item['bid'] . '"') . ' />';
  print PHP_EOL;
}

print '</queue>';

/*$price_auto = db_fetch_array($r);

if (empty($price_auto) || empty($price_auto['prid'])) {
  return;
}

if(db_result(db_query("SELECT COUNT(*) FROM {aad_prices_process_queue}")) > PRICES_QUEUE_TRESHOLD && !isset($_GET['force_price'])) {
  die('Prices queue overflow');
}
if(db_result(db_query("SELECT COUNT(*) FROM {aad_prices_process_queue} WHERE prid = %d", $price_auto['prid'])) > 0) {
  /// Прайс ещё не обработался до конца процессом обработки
  print 'Предыдущая обработка прайса №' . $price_auto['prid'] . ' ещё не была закончена, пожалуйста попробуйте ещё раз через 10 минут' . "<br />\n";
  db_query("UPDATE {aad_prices_auto} SET working = %d WHERE prid = %d", time(), $price_auto['prid']);
  if (isset($_GET['parse_big']) || isset($_GET['force_price'])) {
    exit;
  }
  $prices_parsed_list[] = $price_auto['prid'];
  continue;
}
$prid = $price_auto['prid'];
if (!$prid) {
  print 'Нет прайсов для парсинга';
  exit;
}
/// если прайс занят, ничего не делаем
if(!lock_acquire('prices:parse:' . $prid, PRICES_AUTO_BATCH_IDLE_TIMEOUT)) {
  print "Не удалось получить блокировку на прайс $prid, попробуйте через 20 минут<br />\n";
  if (isset($_GET['parse_big']) || isset($_GET['force_price'])) {
    exit;
  }
  continue;
}

if (db_result(db_query("SELECT pg_try_advisory_lock(%d)", $prid)) !== 't') {
  continue;
}

$__ar = db_fetch_array(db_query('SELECT * FROM {aad_prices_auto} WHERE prid = %d', $prid));
if((time() - $__ar['last_update']) < 300 && !isset($_GET['force_price'])) {
  lock_release('prices:parse:' . $prid);
  print "Прайс $prid уже был обновлён только что <br />\n";
  if (isset($_GET['parse_big']) || isset($_GET['force_price'])) {
    exit;
  }
  continue; /// Прайс уже был обновлён
}*/