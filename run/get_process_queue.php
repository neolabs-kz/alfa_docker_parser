<?php

$_SERVER['HTTP_HOST'] = $_ENV['ALFA_DOMAIN_NAME'];
$_SERVER['DOCUMENT_ROOT'] = '/alfa';
chdir($_SERVER['DOCUMENT_ROOT']);
define('ALFA_ENV', isset($_ENV['ALFA_ENV'])? $_ENV['ALFA_ENV']: 'development');

include_once './includes/bootstrap.inc';
@ drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

module_load_include('inc', 'aad_prices', 'aad_prices.autoparser');

$r = db_query("SELECT id % " . PRICES_AUTO_PROCESS_NUM . " AS proc_id, count(*) FROM {aad_prices_process_queue} GROUP BY id % " . PRICES_AUTO_PROCESS_NUM);

while($ar = db_fetch_array($r)) {
  print ($ar['proc_id'] == 0? 15: $ar['proc_id']) . PHP_EOL;
}
