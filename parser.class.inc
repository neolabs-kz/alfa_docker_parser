<?php

define('PARSER_LOCK_TIMEOUT', 1200); // 20 минут на блокировку максимум

class Parser {
  protected $_locks = array();
  private $lock_id;
  private $db_prefix = '';

  function __construct($redis, $db_prefix = '', $env = 'development') {
    $this->redis = $redis;
    $this->db_prefix = $db_prefix;
    $this->env = $env;

    $this->lock_id = uniqid(mt_rand(), TRUE);
  }

  function __destruct() {
    $this->lock_release_all();
  }

  public function lock_acquire($name, $timeout = PARSER_LOCK_TIMEOUT) {
    // Ensure that the timeout is at least 1 sec. This is a limitation
    // imposed by redis.
    $timeout = (int) max($timeout, 1);
    $result = $this->redis->get($this->db_prefix . 'lock:' . $name);

    if ($result) {
      if (isset($this->_locks[$name]) && $result == $this->lock_id) {
        // Only renew the lock if we already set it and it has not expired.
        $this->redis->expire($this->db_prefix . 'lock:' . $name, $timeout);
      }
      else {
        return FALSE;
      }
    }
    else {
      if ($this->redis->set($this->db_prefix . 'lock:' . $name, $this->lock_id)) {
        $this->redis->expire($this->db_prefix . 'lock:' . $name, $timeout);
        $this->_locks[$name] = $this->lock_id;
      }
      else {
        // Failed to acquire the lock.  Unset the key from the $locks array even if
        // not set, PHP 5+ allows this without error or warning.
        unset($this->_locks[$name]);
      }
    }

    return isset($this->_locks[$name]);
  }

  public function lock_release($name) {
    if ($this->_locks[$name] = $this->lock_id) {
      // Lock is owned by current instance
      $this->redis->del($this->db_prefix . 'lock:' . $name);
      unset($this->_locks[$name]);
      return TRUE;
    }
    return FALSE;
  }

  protected function lock_release_all() {
    if(count($this->_locks) > 0) {
      foreach ($this->_locks as $name => $id) {
        $value = $this->redis->get($this->db_prefix . 'lock:' . $name);
    
        if ($value == $id) {
          $this->redis->delete($this->db_prefix . 'lock:' . $name);
        }
      }
    }
  }

  public function get_process_queue() {
    $command = 'ALFA_ENV=' . $this->env . (empty($this->db_prefix)? '': ' DB_PREFIX=' . $this->db_prefix) . ' php /parser/run/get_process_queue.php';
    //echo $command . PHP_EOL;
    if (class_exists('Co\System')) {
      $ret = Co\System::exec($command);
    }
    else {
      $ret = array('output' => shell_exec($command));
    }

    $processes = explode("\n", trim($ret['output']));

    $processes = array_filter($processes, 'is_numeric');

    if (!is_array($processes)) {
      $processes = array();
    }
    return $processes;
  }

  public function process_all($sync = FALSE) {
    $output = '';
    $total = 0;

    $queue = $this->get_process_queue();

    Swoole\Coroutine\parallel(swoole_cpu_num(), function() use (&$queue, &$total) {
      while(count($queue) > 0) {
        $proc_id = array_pop($queue);

        // при попытке использовать один и тот же редис на несколько потоко;
        // Fatal error: Uncaught Swoole\Error: Socket#23 has already been bound to another coroutine#2, 
        // reading of the same socket in coroutine#3 at the same time is not allowed in /parser/parser.class.inc:26
        //$lock_id = 'parser:process:' . $proc_id;
        //if ($this->lock_acquire($lock_id, PARSER_LOCK_TIMEOUT)) {
        $command = 'ALFA_ENV=' . $this->env . (empty($this->db_prefix)? '': ' DB_PREFIX=' . $this->db_prefix) . 
                   ' php /parser/run/exec_process.php ' . $proc_id;

        //echo $command . PHP_EOL;
        $ret = Co\System::exec($command);
        $output .= $lock_id . ': ' . $ret['output'] . PHP_EOL;
        if ($ret['code'] == 0) {
          $total += intval(trim($ret['output']));
        }
        else {
          echo "Error: " . $ret['output'];
        }
      }
    });

    return $total;
  }

  public function get_parser_queue() {
    $command = 'ALFA_ENV=' . $this->env . (empty($this->db_prefix)? '': ' DB_PREFIX=' . $this->db_prefix) . 
               ' php /parser/run/get_parser_queue.php ' . $proc_id;

    $ret = Co\System::exec($command);

    $doc = new DOMDocument('1.0');
    $doc->loadXML($ret['output']);

    foreach($doc->getElementsByTagName('error') as $error) {
      throw new Exception($error->getText());
    }

    $result = array();
    foreach($doc->getElementsByTagName('item') as $item) {
      $item_ar = array(
        'prid' => $item->getAttribute('prid'),
        'is_big' => $item->getAttribute('is_big'),
      );
      if($item->hasAttribute('bid')) {
        $item_ar['bid'] = $item->getAttribute('bid');
      }
      $result[] = $item_ar;
    }

    return $result;
  }

  public function exec_parse($prid, $force_diff = FALSE) {
    $lock_id = 'parser:parse:' . $prid;
    if ($this->lock_acquire($lock_id, PARSER_LOCK_TIMEOUT)) {

      $cache_dir = $_ENV['ALFA_PARSER_CACHE_DIR'] . '/' . $this->env . (empty($this->db_prefix)? '': '/' . $this->db_prefix) . '/' . $prid;

      if (!is_dir($cache_dir)) {
        print 'Creating dir ' . $cache_dir . PHP_EOL;
        mkdir($cache_dir, 0777, TRUE);
      }

      if (!file_exists($cache_dir . '/shops.dtd')) {
        // DTD-файл необходим для валидации YML-документов.
        print 'copying ' . dirname(__FILE__) . '/shops.dtd to ' . $cache_dir . '/shops.dtd' . PHP_EOL;
        copy(dirname(__FILE__) . '/shops.dtd', $cache_dir . '/shops.dtd');
      }

      $command = 'ALFA_ENV=' . $this->env . (empty($this->db_prefix)? '': ' DB_PREFIX=' . $this->db_prefix) . ($force_diff? ' ALFA_FORCE_DIFF=1': '') . 
                 ' php /parser/run/exec_parse.php ' . $prid;


      $output = 'NEXTCOMMAND: ' . $command;
      $matches = array();

      while(preg_match('/NEXTCOMMAND: (.*)/', $output, $matches)) {
        print 'Couroutine running: ' . $matches[1] . PHP_EOL;
        go(function() use ($matches, &$output) {
          $output = system($matches[1]);
        });
        print '=====================================' . PHP_EOL;
        if (strpos($matches[1], ' finished ') !== FALSE) {
          break;
        }
      }

      return;
    }
    else {
      return 'Cannot acquire lock for prid=' . $prid . PHP_EOL;
    }
  }

  public function process_images() {
    $lock_id = 'parser:images';
    if ($this->lock_acquire($lock_id, PARSER_LOCK_TIMEOUT)) {


      $command = 'ALFA_ENV=' . $this->env . (empty($this->db_prefix)? '': ' DB_PREFIX=' . $this->db_prefix) . 
                 ' php /parser/run/exec_images.php';

      print $command . PHP_EOL;

      $ret = Co\System::exec($command);

      if ($this->env != 'production') print $ret['output'];

      $this->lock_release($lock_id);

      return $ret['output'];
    }
    else {
      return 'Cannot acquire lock for images' . PHP_EOL;
    }
  }

  public function cleanup() {
    if ($this->env == 'testing') {
      $command = 'rm -rf ' . $_ENV['ALFA_PARSER_CACHE_DIR'] . '/' . $this->env . '/' . (empty($this->db_prefix)? '*': $this->db_prefix);
      $ret = Co\System::exec($command . ' && find /alfa/' . $_ENV['ALFA_CONF_FILE_DIRECTORY_PATH'] . '/' . (empty($this->db_prefix)? 'simpletest*': $this->db_prefix) . ' -delete');

      return $ret['output']? $ret['output']: 'success';
    }
  }
}