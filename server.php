<?php

// интервал опроса парсера, как часто будет производиться запрос на получение обновлений, секунд
define('ALFA_PARSER_HEARTBEAT_TIMEOUT', 60);
define('REDIS_DATABASE_INDEXER', 11);
define('REDIS_ERROR_MSG', 'You should specify Redis server in ALFA_CONF_REDIS_SERVER in format host:port');
define('PARSER_HEALTH_FILE', '/parser/health');

list($redis_host, $redis_port) = explode(':', $_ENV['ALFA_CONF_REDIS_SERVER']);
if (empty($redis_host) || empty($redis_port)) {
  die(REDIS_ERROR_MSG);
}

include_once('parser.class.inc');

$server = new Swoole\HTTP\Server("0.0.0.0", $_ENV['ALFA_PARSER_PORT']);

touch(PARSER_HEALTH_FILE);

$server->on("start", function (Swoole\Http\Server $server)  use ($redis_host, $redis_port) {
  echo "Swoole http server for parser is started at http://0.0.0.0:{$_ENV['ALFA_PARSER_PORT']}\n";

  go(function() use($redis_host, $redis_port) {
    $redis = new Swoole\Coroutine\Redis();
    $redis->connect($redis_host, $redis_port);
    $redis->select(REDIS_DATABASE_INDEXER);

    $parser = new Parser($redis, '', $_ENV['ALFA_ENV']);

    $errors = 0;

    while(true) {
      try {
        $total = $parser->process_all();
        if ($total > 0) {
          print 'processed items: ' . $total; print PHP_EOL;
        }
        $errors = 0;
        touch(PARSER_HEALTH_FILE);
      }
      catch(Exception $e) {
        print '[' . date('d/M/Y:H:i:s O') . '] Exception: ' . $e->getMessage() . PHP_EOL;
        $errors++;
      }
      if ($errors > 0) {
        print '[' . date('d/M/Y:H:i:s O') . '] errors: ' . $errors . PHP_EOL;
      }
      Co::sleep(ALFA_PARSER_HEARTBEAT_TIMEOUT * ($errors + 1));
    }
  });

});

$server->on("request", function (Swoole\Http\Request $request, Swoole\Http\Response $response) use ($redis_host, $redis_port) {
  if ($request->server['request_uri'] == '/favicon.ico') {
    $response->end("");
    return;
  }

  $get_query = NULL;
  if (!empty($request->get)) {
    $get_query = http_build_query($request->get);
  }
  echo "{$request->server['request_method']} request {$request->header['host']}{$request->server['request_uri']}" . (empty($get_query)? '': '?' . $get_query) . PHP_EOL;
  // Start new instance with given params for testing purposes
  if ($request->server['request_uri'] == '/parse') {
    $db_prefix = $request->get['db_prefix'];
    $env = $request->get['env'];
    $prid = $request->get['prid'];
    $sync = !empty($request->get['sync']);
    $force_diff = !empty($request->get['force_diff']);

    $text = "Parsing price #$prid, db_prefix=$db_prefix\n";
    echo $text;

    $redis = new Swoole\Coroutine\Redis();
    $redis->connect($redis_host, $redis_port);
    $redis->select(REDIS_DATABASE_INDEXER);

    $parser = new Parser($redis, $db_prefix, $env);

    if ($sync) {
      $response->header("Content-Type", "text/plain");
      $response->end($parser->exec_parse($prid, $force_diff));
    }
    else {
      Co\run(function() use($parser, $prid, $force_diff) {
        echo $parser->exec_parse($prid, $force_diff);
        $response->header("Content-Type", "text/plain");
      });
      $response->end("(async) " . $text);
    }

  }
  else if ($request->server['request_uri'] == '/process') {
    $db_prefix = $request->get['db_prefix'];
    $env = $request->get['env'];
    $sync = !empty($request->get['sync']);

    $text = "Process prices, db_prefix=$db_prefix\n";
    echo $text;

    $redis = new Swoole\Coroutine\Redis();
    $redis->connect($redis_host, $redis_port);
    $redis->select(REDIS_DATABASE_INDEXER);

    $parser = new Parser($redis, $db_prefix, $env);

    $response->header("Content-Type", "text/plain");
    if ($sync) {
      $response->end($parser->process_all($sync));
    }
    else {
      Co\run(function() use($parser) {
        $parser->process_all();
      });
      $response->end($text);
    }
  }
  else if ($request->server['request_uri'] == '/images') {
    $db_prefix = $request->get['db_prefix'];
    $env = $request->get['env'];
    $sync = !empty($request->get['sync']);

    $text = "Process images, db_prefix=$db_prefix\n";
    echo $text;

    $redis = new Swoole\Coroutine\Redis();
    $redis->connect($redis_host, $redis_port);
    $redis->select(REDIS_DATABASE_INDEXER);

    $parser = new Parser($redis, $db_prefix, $env);

    $response->header("Content-Type", "text/plain");
    if ($sync) {
      $response->end($parser->process_images());
    }
    else {
      Co\run(function() use($parser) {
        $parser->process_images();
      });
      $response->end($text);
    }
  }
  else if ($request->server['request_uri'] == '/queue') {
    $db_prefix = $request->get['db_prefix'];
    $env = $request->get['env'];
    $sync = !empty($request->get['sync']);

    $text = "Process prices, db_prefix=$db_prefix\n";
    echo $text;

    $redis = new Swoole\Coroutine\Redis();
    $redis->connect($redis_host, $redis_port);
    $redis->select(REDIS_DATABASE_INDEXER);

    $parser = new Parser($redis, $db_prefix, $env);

    $response->header("Content-Type", "application/xml");

    $output = '<?xml version="1.0" encoding="utf-8"?>' . PHP_EOL;
    try {
      $queue = $parser->get_parser_queue($sync);

      $output .= '<queue>' . PHP_EOL;
      foreach($queue as $item) {
        $output .= '<item prid="' . $item['prid'] . '" is_big="' . $item['is_big'] . '"' . (empty($item['bid'])? '': ' bid="' . $item['bid'] . '"') . ' />';
        $output .= PHP_EOL;
      }
      $output .= '</queue>' . PHP_EOL;
    }
    catch(Exception $e) {
      $output = '<error>' . $e->getMessage() . '</error>';
    }


    $response->end($output);
  }
  else if ($request->server['request_uri'] == '/cleanup') {
    $db_prefix = $request->get['db_prefix'];
    $env = $request->get['env'];
    $sync = !empty($request->get['sync']);

    $redis = new Swoole\Coroutine\Redis();
    $redis->connect($redis_host, $redis_port);
    $redis->select(REDIS_DATABASE_INDEXER);

    $parser = new Parser($redis, $db_prefix, $env);

    $response->header("Content-Type", "text/plain");
    $response->end($parser->cleanup());
  }
});

$server->start();

use Swoole\Coroutine as Co;

Co\run(function() use ($redis_host, $redis_port) {
  $redis = new Swoole\Coroutine\Redis();
  $redis->connect($redis_host, $redis_port);
  $redis->select(REDIS_DATABASE_INDEXER);

  $parser = new Parser($redis, '', $_ENV['ALFA_ENV']);


  Swoole\Timer::tick(ALFA_PARSER_HEARTBEAT_TIMEOUT * 1000, function () use ($parser) {
    Co\run(function() use($parser) {
      $parser->process_all();
    });
  });

});
