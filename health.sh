#!/bin/sh

if test `find "/parser/health" -mmin -120`
then
    exit 0
else
    exit 1
fi
